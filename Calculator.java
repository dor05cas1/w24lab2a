import java.util.Random;
public class Calculator
{
	public static int sumNumbers(int num1, int num2)
	{
		return num1 + num2;
	}
	public static double squareRoot(int sqrtNum)
	{
		
		return Math.sqrt(sqrtNum);
	}
	public static int randomNumber()
	{
		Random rando = new Random();
		int randNum = rando.nextInt(8888);
		return randNum;
	}
	public static int divideNumbers(int divNum1, int divNum2)
	{
		if (divNum2 == 0)
		{
			return -1;
		}
		return divNum1/divNum2;
	}
}