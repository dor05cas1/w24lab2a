import java.util.Scanner;

public class Driver
{
	public static void main (String args[])
	{
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello user");
		
		//calculating: sum of 2 numbers
		System.out.println("Sum! Type the first number to add up");
		int num1 = reader.nextInt();
		
		System.out.println("Type the second number to add up");
		int num2 = reader.nextInt();
		
		int sum = Calculator.sumNumbers(num1, num2);
		System.out.println("The sum of both numbers = " + sum);
		
		//calculating: square root of a number
		System.out.println("Square root! Type a number and get the square root!");
		int sqrtNum = reader.nextInt();
		
		double sqrtResult = Calculator.squareRoot(sqrtNum);
		System.out.println("The square root of the number you typed is = " + sqrtResult);
		
		//give the user a random number
		int randNum = Calculator.randomNumber();
		System.out.println("Random number? Here is a random number for some reason: " + randNum);
		
		//was not required but i wanted to do it anyway
		//calculating: division of 2 numbers
		System.out.println("Division (you will get an approximation sorry no decimals ;;)! Type the dividend");
		int divNum1 = reader.nextInt();
		
		System.out.println("Type the divisor of the dividend you previously entered");
		int divNum2 = reader.nextInt();
		
		if (divNum2 == 0)
		{
			System.out.println("Dividing by 0 will result in an error... exiting...");
		}
			int quotient = Calculator.divideNumbers(divNum1, divNum2);
			System.out.println("Quotient = " + quotient);
		
		reader.close();
		
	}
}